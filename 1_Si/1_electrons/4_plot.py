#!/usr/bin/env python

import matplotlib; matplotlib.use('tkagg')
import matplotlib.pyplot as plt
import elphmod

k, eps = elphmod.el.read_bands_plot('bands.dat.gnu', bands=8)
x = elphmod.el.read_symmetry_points('3_bands.out')

plt.plot(k, eps, color='red')
plt.xticks(x, 'GLXG')
plt.xlabel('wave vector')
plt.ylabel('electron energy (eV)')
plt.show()

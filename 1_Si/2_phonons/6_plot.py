#!/usr/bin/env python

import numpy as np
import matplotlib; matplotlib.use('tkagg')
import matplotlib.pyplot as plt
import elphmod

bnd = np.loadtxt('bands.dat.gp')
dos = np.loadtxt('dos.dat', skiprows=1)

fig, (ax1, ax2) = plt.subplots(1, 2, sharey=True)

ax1.set_ylabel(r'phonon frequency ($\mathrm{cm}^{-1}$)')
ax1.set_xlabel(r'wave vector ($2 \pi / a$)')
ax2.set_xlabel(r'density of states ($1 / \mathrm{cm}^{-1}$)')

ax1.plot(bnd[:, 0], bnd[:, 1:], color='red')
ax2.fill(dos[:, 1], dos[:, 0 ], color='gray')

plt.show()

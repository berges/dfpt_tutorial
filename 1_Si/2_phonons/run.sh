#!/bin/bash

. ../../environment.sh

# 1. Perform DFT calculation:

mpirun -np $NP pw.x -nk $NK < 1_pw.in | tee 1_pw.out

# 2. Perform DFPT calculation:

mpirun -np $NP ph.x -nk $NK < 2_ph.in | tee 2_ph.out

# 3. Calculate interatomic force constants from dynamical matrices:

q2r.x < 3_q2r.in | tee 3_q2r.out

# 4./5. Calculate phonon dispersion/DOS from interatomic force constants:

matdyn.x < 4_bnd.in | tee 4_bnd.out
matdyn.x < 5_dos.in | tee 5_dos.out

# 6. Show plotted phonon dispersion:

$CONDA/python 6_plot.py

#!/bin/bash
#SBATCH --nodes 1
#SBATCH --tasks-per-node 96
#SBATCH --partition standard96
#SBATCH --time 12:00:00

module load intel
module load impi

export SLURM_CPU_BIND=none

cd $SLURM_SUBMIT_DIR

mpirun pw.x -nk 4 < 1_scf.in > 1_scf.out
mpirun pw.x -nk 4 < 2_relax.in > 2_relax.out

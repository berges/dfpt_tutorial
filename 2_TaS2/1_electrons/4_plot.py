#!/usr/bin/env python

import matplotlib; matplotlib.use('tkagg')
import matplotlib.pyplot as plt
import elphmod

k, eps = elphmod.el.read_bands_plot('bands.dat.gnu', bands=13)
mu = elphmod.el.read_Fermi_level('1_scf.out')
x = elphmod.el.read_symmetry_points('3_bands.out')

plt.axhline(mu, color='gray')
plt.plot(k, eps, color='red')
plt.xticks(x, 'GMKG')
plt.xlabel('wave vector')
plt.ylabel('electron energy (eV)')
plt.show()

#!/usr/bin/env python

import numpy as np
import matplotlib; matplotlib.use('tkagg')
import matplotlib.pyplot as plt
import elphmod

GHz2cmm1 = 0.0333564
scale = 4.0

bnd = np.loadtxt('bands.dat.gp').T
dos = np.loadtxt('dos.dat',  skiprows=1).T
a2f = np.loadtxt('a2F.dos2', skiprows=5, comments='l').T
q, gamma = elphmod.el.read_bands('elph.gamma.2')
gamma *= GHz2cmm1

a2f[1] *= dos[1].max() / a2f[1].max()

fig, (ax1, ax2) = plt.subplots(1, 2, sharey=True)

ax1.set_ylabel(r'phonon frequency ($\mathrm{cm}^{-1}$)')
ax1.set_xlabel(r'wave vector ($2 \pi / a$)')

for nu in range(9):
    ax1.fill_between(bnd[0],
        bnd[nu + 1] - scale * gamma[nu] / 2,
        bnd[nu + 1] + scale * gamma[nu] / 2, color='orange',
        label=None if nu else r'$%g \gamma$' % scale)

    ax1.plot(bnd[0], bnd[nu + 1], color='black')

ax2.fill(dos[1], dos[0], color='gray',   label=r'DOS ($1 / \mathrm{cm}^{-1}$)')
ax2.fill(a2f[1], dos[0], color='orange', label=r'$\alpha^2 F$ (arb. units)')

ax1.legend(); ax2.legend()
plt.show()

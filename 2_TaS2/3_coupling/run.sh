#!/bin/bash

. ../../environment.sh

# 1./2. Perform DFT calculation on dense and coarse k mesh:

mpirun -np $NP pw.x -nk $NK < 1_pw.in | tee 1_pw.out
mpirun -np $NP pw.x -nk $NK < 2_pw.in | tee 2_pw.out

# 3. Perform DFPT calculation:

mpirun -np $NP ph.x -nk $NK < 3_ph.in | tee 3_ph.out

# 4. Calculate interatomic force constants from dynamical matrices:

q2r.x < 4_q2r.in | tee 4_q2r.out

# 5./6. Calculate phonon dispersion/DOS from interatomic force constants:

matdyn.x < 5_bnd.in | tee 5_bnd.out
matdyn.x < 6_dos.in | tee 6_dos.out

# 7. Plot phonon linewidth and Eliashberg spectral function:

$CONDA/python 7_plot.py

#!/bin/bash

cd 1_Si

cd 1_electrons
./run.sh
cd ..

cd 2_phonons
./run.sh
cd ..

cd ..

cd 2_TaS2

cd 1_electrons
./run.sh
cd ..

cd 2_phonons
./run.sh
cd ..

cd 3_coupling
./run.sh
cd ..

cd ..

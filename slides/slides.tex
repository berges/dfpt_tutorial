\documentclass[aspectratio=169, serif]{beamer}

\setbeamertemplate{navigation symbols}{}

\usepackage[english]{babel}
\usepackage[utf8]{inputenc}

\usepackage{concmath}
\usepackage[T1]{fontenc}

\pdfpkresolution=2400
\pdfpkmode={supre}

\usefonttheme{professionalfonts}

\usepackage{mathtools, graphicx, tikz, bm, upgreek, listings}

\definecolor{1}{RGB}{233, 89, 37}
\definecolor{2}{RGB}{224, 0, 26}
\definecolor{3}{RGB}{176, 15, 30}

\usecolortheme[named=2]{structure}
\setbeamerfont{framesubtitle}{size=\large}

\title{Electrons and phonons of Si and H-TaS2$_2$\\
  calculated using Quantum ESPRESSO}
\author{Jan Berges, Tim Wehling, Christof Köhler}
\date{10 January 2019}

\let\vec\bm
\let\epsilon\varepsilon
\let\Phi\varPhi

\def\D{\mathrm d}
\def\E{\mathrm e}
\def\I{\mathrm i}

\def\sub#1{\sb{\text{#1}}}
\def\super#1{\sp{\text{#1}}}

\def\bra#1{\langle#1|}
\def\ket#1{|#1\rangle}
\def\bracket#1#2{\langle#1|#2\rangle}
\def\av#1{\langle#1\rangle}

\def\step#1{\textcolor{1}{(#1)}}
\def\sh#1:{$\blacktriangleright$}

\lstset{
    basicstyle = \color{2} \scriptsize,
    keywordstyle = \color{3},
    identifierstyle = \color{black},
    commentstyle = \color{1},
    showstringspaces = false,
    }


\def\footer#1{%
  \begin{tikzpicture}[remember picture, overlay]
    \node [above=-1pt, fill=1!30] at (current page.south) {
      \begin{minipage}{\paperwidth}
        \hspace*{2mm}#1\hspace*{2mm}%
      \end{minipage}%
      };
  \end{tikzpicture}
  }

\def\doc#1{%
  \begin{tikzpicture}[remember picture, overlay]
    \node [below left] at (current page.north east)
      { \it \color{1} $\rightarrow$ #1 };
  \end{tikzpicture}
  }

\def\popup<#1>#2{%
  \begin{tikzpicture}[remember picture, overlay]
    \uncover<#1>{
      \fill [gray, fill opacity=0.4]
        (current page.south west) rectangle (current page.north east);
      \node [draw, fill=white, rounded corners, inner sep=2mm, align=center]
        at (current page.center)
        { \includegraphics[width=0.7\textwidth]{#2} };
      }
  \end{tikzpicture}
  }

\begin{document}
  \begin{frame}
    \thispagestyle{empty}
    \maketitle
    \footer{%
      \includegraphics[height=5mm]{fig/uni.pdf}%
      \hfill%
      \raisebox{1ex}{\it RTG QM3 --- Hands-on tutorial}%
      \hfill%
      \includegraphics[height=5mm]{fig/dfg.pdf}%
      }
  \end{frame}

  \begin{frame}{Structure}
    \let\olditem\item
    \def\item{\olditem\vspace{-0.6ex}}
    \begin{minipage}[c][0.7\textheight]{0.4\textwidth}
      \tableofcontents
    \end{minipage}%
    \begin{minipage}[c][0cm]{0.3\textwidth}
      \small
      \begin{itemize}
        \item 1\_Si
        \begin{itemize}
          \item 1\_electrons
          \begin{itemize}
            \item 1\_scf.in
            \item 2\_nscf.in
            \item 3\_bands.in
            \item 4\_plot.py
            \item run.sh
          \end{itemize}
          \item 2\_phonons
          \begin{itemize}
            \item 1\_pw.in
            \item 2\_ph.in
            \item 3\_q2r.in
            \item 4\_bnd.in
            \item 5\_dos.in
            \item 6\_plot.py
            \item run.sh
          \end{itemize}
          \item pp
          \begin{itemize}
            \item Si.vbc.UPF
          \end{itemize}
        \end{itemize}
        \item environment.sh
        \item run\_all.sh
      \end{itemize}
    \end{minipage}%
    \begin{minipage}[c][0cm]{0.4\textwidth}
      \small
      \begin{itemize}
        \item 2\_TaS2
        \begin{itemize}
          \item 1\_electrons
          \begin{itemize}
            \item 1\_scf.in
            \item 2\_nscf.in
            \item 3\_bands.in
            \item 4\_plot.py
            \item run.sh
          \end{itemize}
          \item 2\_phonons
          \begin{itemize}
            \item 1\_pw.in
            \item 2\_ph.in
            \item 3\_q2r.in
            \item 4\_plot.py
            \item run.sh
          \end{itemize}
          \item 3\_coupling
          \begin{itemize}
            \item 1\_pw.in
            \item 2\_pw.in
            \item 3\_ph.in
            \item 4\_q2r.in
            \item 5\_bnd.in
            \item 6\_dos.in
            \item 7\_plot.py
            \item run.sh
          \end{itemize}
          \item pp
          \begin{itemize}
            \item S.pbe-hgh.UPF
            \item Ta.pbe-hgh.UPF
          \end{itemize}
        \end{itemize}
      \end{itemize}
    \end{minipage}%
  \end{frame}

  \section{Prerequisites}

  \begin{frame}{Prerequisites}
    \begin{itemize}
      \item wget https://berges.canopus.uberspace.de/dfpt\_tutorial.pdf
      \bigskip
      \item ssh -X qm3grk@\dots
      \item cd /scratch/qm3grk
      \item git clone https://berges@bitbucket.org/berges/dfpt\_tutorial.git \textit{\color1userXXX}
      \bigskip
      \item ssh -X node\textit{\color1XXX}
      \item cd /scratch/qm3grk
      \item cd \textit{\color1userXXX}/1\_Si/1\_electrons
      \item ./run.sh
      \item \dots
    \end{itemize}
  \end{frame}

  \section{What is Quantum ESPRESSO?}

  \begin{frame}{What is Quantum ESPRESSO?}
    \begin{itemize}
      \item Framework
      \begin{itemize}
        \item Density-functional theory
        \item Plane-wave basis
        \item Pseudopotentials
      \end{itemize}
      \item Organization
      \begin{itemize}
        \item Collection of core and plug-in codes
        \item Open source
      \end{itemize}
      \item Resources
      \begin{itemize}
        \item https://www.quantum-espresso.org
          \textcolor{1}{\it (documentation, PPs)}
        \item https://gitlab.com/QEF/q-e.git
          \textcolor{1}{\it (versions tagged)}
        \item http://epw.org.uk/Documentation/School2018
          \textcolor{1}{\it (tutorial)}
      \end{itemize}
      \item Development
      \begin{itemize}
        \item Since 2001
          \textcolor{1}{\it (pw.1.0.0, $\rightarrow$ www.quantum-espresso.org/previous-versions)}
        \item Coordinated by Quantum ESPRESSO Foundation
          \textcolor{1}{\it (SISSA, ICTP, EPFL, \dots)}
        \item Everyone can contribute
      \end{itemize}
    \end{itemize}
    \begin{tikzpicture}[remember picture, overlay]
      \node [below left=5mm] at (current page.north east)
        {\includegraphics[width=6cm]{fig/qe.pdf}};
    \end{tikzpicture}
  \end{frame}

  \section{Example materials}

  \begin{frame}{Example materials}
    \begin{minipage}[t]{0.5\textwidth}
      \centering
      {\Large \color{2} Si \color{1} (Semiconductor)}\\
      \includegraphics[width=0.9\textwidth]{fig/si.png}
    \end{minipage}%
    \begin{minipage}[t]{0.5\textwidth}
      \centering
      {\Large \color{2} TaS$_2$ \color{1} (Metal)}\\
      \includegraphics[width=\textwidth]{fig/tas2.png}
    \end{minipage}%
  \end{frame}

  \section{Si}

  \subsection{Electrons}

  \begin{frame}{Electrons of Si}
    \begin{enumerate}
      \item Calculate charge-density self-consistently on full Brillouin zone
      \item Calculate energies non-self-consistently along high-symmetry path
      \item Convert binary data into human-readable format
      \item Visualize results
    \end{enumerate}
    \vspace{1cm}
    \small
    \textcolor{1}{\it (Example adapted from tutorial by Stefano de Gironcoli,\\
    $\rightarrow$ http://epw.org.uk/School2018/School2018\\
    $\phantom \rightarrow$ ?action=downloadman\&upname=Mon.5.Gironcoli.tgz)}
  \end{frame}

  \begin{frame}{Electrons of Si}
      {\step 1 \textit{pw.x} --- Self-consistent DFT calculation}
    \begin{minipage}[t]{0.4\textwidth}
      \lstinputlisting[lastline=17]{../1_Si/1_electrons/1_scf.in}
    \end{minipage}%
    \begin{minipage}[t]{0.6\textwidth}
      \lstinputlisting[firstline=18]{../1_Si/1_electrons/1_scf.in}
    \end{minipage}%
    \doc{www.quantum-espresso.org/Doc/INPUT\_PW.html}
    \footer{\sh: mpirun -np 20 pw.x -nk 10 < 1\_scf.in | tee 1\_scf.out \hfill 1.18\,s}
  \end{frame}

  \begin{frame}{Electrons of Si}
    {\step 2 \textit{pw.x} --- Non-self-consistent DFT calculation}
    \begin{minipage}[t]{0.4\textwidth}
      \lstinputlisting[lastline=17]{../1_Si/1_electrons/2_nscf.in}
    \end{minipage}%
    \begin{minipage}[t]{0.6\textwidth}
      \lstinputlisting[firstline=18]{../1_Si/1_electrons/2_nscf.in}
    \end{minipage}%
    \doc{www.quantum-espresso.org/Doc/INPUT\_PW.html}
    \footer{\sh: mpirun -np 20 pw.x -nk 10 < 2\_nscf.in | tee 2\_nscf.out \hfill 1.06\,s}
  \end{frame}

  \begin{frame}{Electrons of Si}
    {\step 3 \textit{bands.x} --- Calculate band structure}
    \lstinputlisting{../1_Si/1_electrons/3_bands.in}
    \doc{www.quantum-espresso.org/Doc/INPUT\_BANDS.html}
    \footer{\sh: mpirun -np 20 bands.x -nk 10 < 3\_bands.in | tee 3\_bands.out \hfill 0.25\,s}
  \end{frame}

  \begin{frame}{Electrons of Si}
    {\step 4 Plot band structure}
    \lstinputlisting[language=Python]{../1_Si/1_electrons/4_plot.py}
    \doc{bitbucket.org/berges/elphmod}
    \footer{\sh: python 4\_plot.py}
    \popup<2>{fig/si-el.pdf}
  \end{frame}

  \subsection{Phonons}

  \begin{frame}{Phonons of Si}
    \begin{enumerate}
      \item Calculate charge-density self-consistently on full Brillouin zone
      \item Calculate dynamical matrices from response to atomic displacements
      \item Transform from dynamical matrices to interatomic force constants
      \item[4./5.] Transform back to reciprocal space (``Fourier interpolation'')
      \item[6.] Plot phonon dispersion and density of states
    \end{enumerate}
    \vspace{1cm}
    \small
    \textcolor{1}{\it (Example adapted from tutorial by Stefano de Gironcoli,\\
    $\rightarrow$ http://epw.org.uk/School2018/School2018\\
    $\phantom \rightarrow$ ?action=downloadman\&upname=Mon.5.Gironcoli.tgz)}
  \end{frame}

  \begin{frame}{Phonons of Si}
      {\step 1 \textit{pw.x} --- Self-consistent DFT calculation}
    \begin{minipage}[t]{0.4\textwidth}
      \lstinputlisting[lastline=17]{../1_Si/2_phonons/1_pw.in}
    \end{minipage}%
    \begin{minipage}[t]{0.6\textwidth}
      \lstinputlisting[firstline=18]{../1_Si/2_phonons/1_pw.in}
    \end{minipage}%
    \doc{www.quantum-espresso.org/Doc/INPUT\_PW.html}
    \footer{\sh: mpirun -np 20 pw.x -nk 10 < 1\_pw.in | tee 1\_pw.out \hfill 0.63\,s}
  \end{frame}

  \begin{frame}{Phonons of Si}
      {\step 2 \textit{ph.x} --- DFPT calculation}
    \lstinputlisting{../1_Si/2_phonons/2_ph.in}
    \doc{www.quantum-espresso.org/Doc/INPUT\_PH.html}
    \footer{\sh: mpirun -np 20 ph.x -nk 10 < 2\_ph.in | tee 2\_ph.out \hfill 4\,min 7.19\,s}
  \end{frame}

  \begin{frame}{Phonons of Si}
      {\step 3 \textit{q2r.x} --- Interatomic force constants from dynamical matrices}
    \begin{minipage}[t][7cm]{0.5\textwidth}
      \vspace*{\fill}
      \lstinputlisting{../1_Si/2_phonons/3_q2r.in}
      \vspace*{\fill}
    \end{minipage}%
    \begin{minipage}[t][7cm]{0.5\textwidth}
      \begin{itemize}
        \item Dynamical matrix
        %
        \begin{align*}
          \textcolor{2}{D_{\vec q i x j y}}
          = \frac 1 {\sqrt{M_i \, M_j}} \sum_{\vec R}
          \E^{\I \vec q \vec R} \,
          \textcolor{1}{\Phi_{\vec R i x j y}}
        \end{align*}
        %
        \item Interatomic force constants
        %
        \begin{align*}
          \textcolor{1}{\Phi_{\vec R i x j y}}
          = \frac {\sqrt{M_i \, M_j}} N \sum_{\vec q}
          \E^{-\I \vec q \vec R} \,
          \textcolor{2}{D_{\vec q i x j y}}
        \end{align*}
        %
        \item 2nd derivative of SCF potential
        %
        \begin{align*}
          \textcolor{1}{\Phi_{\vec R - \vec R' i x j y}}
          = \frac
            {\partial^2 V}
            {\partial u_{\vec R i x} \,
             \partial u_{\vec R' j y}}
        \end{align*}
      \end{itemize}
    \end{minipage}
    \doc{q-e/PHonon/PH/q2r.f90}
    \footer{\sh: q2r.x < 3\_q2r.in | tee 3\_q2r.out \hfill 0.04\,s}
  \end{frame}

  \begin{frame}{Phonons of Si}
    \only<1>{\framesubtitle{\step 4 \textit{matdyn.x} --- Phonon dispersion from force constants}}%
    \only<2>{\framesubtitle{\step 5 \textit{matdyn.x} --- Phonon density of states from force constants}}%
    \begin{minipage}[t][7cm]{0.5\textwidth}
      \only<1>{\lstinputlisting{../1_Si/2_phonons/4_bnd.in}}%
      \only<2>{\lstinputlisting{../1_Si/2_phonons/5_dos.in}}%
    \end{minipage}%
    \begin{minipage}[t][7cm]{0.5\textwidth}
      \begin{itemize}
        \item Dynamical matrix
        %
        \begin{align*}
          \textcolor{2}{D_{\vec q i x j y}}
          = \frac 1 {\sqrt{M_i \, M_j}} \sum_{\vec R}
          \E^{\I \vec q \vec R} \,
          \textcolor{1}{\Phi_{\vec R i x j y}}
        \end{align*}
        %
        \item Interatomic force constants
        %
        \begin{align*}
          \textcolor{1}{\Phi_{\vec R i x j y}}
          = \frac {\sqrt{M_i \, M_j}} N \sum_{\vec q}
          \E^{-\I \vec q \vec R} \,
          \textcolor{2}{D_{\vec q i x j y}}
        \end{align*}
        %
        \item Phonon frequencies
        %
        \begin{align*}
          \sum_{j y} \textcolor{2}{D_{\vec q i x j y}} \,
          e_{\vec q j y \nu}
          = \textcolor{3}{\omega^2_{\vec q \nu}} \,
          e^{\phantom 2}_{\vec q i x \nu}
        \end{align*}
      \end{itemize}
    \end{minipage}
    \doc{q-e/PHonon/PH/matdyn.f90}
    \only<1>{\footer{\sh: matdyn.x < 4\_bnd.in | tee 4\_bnd.out \hfill 0.89\,s}}%
    \only<2>{\footer{\sh: matdyn.x < 5\_dos.in | tee 5\_dos.out \hfill 3.43\,s}}%
  \end{frame}

  \begin{frame}{Phonons of Si}
      {\step 6 Plot dispersion and density of states}
    \lstinputlisting[language=Python]{../1_Si/2_phonons/6_plot.py}
    \doc{bitbucket.org/berges/elphmod}
    \footer{\sh: python 6\_plot.py}
    \popup<2>{fig/si-ph.pdf}
  \end{frame}

  \section{TaS$_2$}

  \subsection{Electrons}

  \begin{frame}{Electrons of TaS$_2$}
    \begin{enumerate}
      \item Calculate charge-density self-consistently on full Brillouin zone
      \item Calculate energies non-self-consistently along high-symmetry path
      \item Convert binary data into human-readable format
      \item Visualize results
    \end{enumerate}
    \begin{tikzpicture}[remember picture, overlay]
      \node [draw=2, color=2, rotate=30, fill=white, rounded corners, inner sep=2mm, align=center]
        at (current page.center)
        { Same procedure as before };
    \end{tikzpicture}
  \end{frame}

  \begin{frame}{Electrons of TaS$_2$}
      {\step 1 \textit{pw.x} --- Self-consistent DFT calculation}
    \vspace*{-4mm}
    \begin{minipage}[t]{0.4\textwidth}
      \lstinputlisting[lastline=20]{../2_TaS2/1_electrons/1_scf.in}
    \end{minipage}%
    \begin{minipage}[t]{0.6\textwidth}
      \lstinputlisting[firstline=21]{../2_TaS2/1_electrons/1_scf.in}
    \end{minipage}%
    \doc{www.quantum-espresso.org/Doc/INPUT\_PW.html}
    \footer{\sh: mpirun -np 20 pw.x -nk 10 < 1\_scf.in | tee 1\_scf.out \hfill 3.98\,s}
  \end{frame}

  \begin{frame}{Electrons of TaS$_2$}
    {\step 2 \textit{pw.x} --- Non-self-consistent DFT calculation}
    \vspace*{-4mm}
    \begin{minipage}[t]{0.4\textwidth}
      \lstinputlisting[lastline=20]{../2_TaS2/1_electrons/2_nscf.in}
    \end{minipage}%
    \begin{minipage}[t]{0.6\textwidth}
      \lstinputlisting[firstline=21]{../2_TaS2/1_electrons/2_nscf.in}
    \end{minipage}%
    \doc{www.quantum-espresso.org/Doc/INPUT\_PW.html}
    \footer{\sh: mpirun -np 20 pw.x -nk 10 < 2\_nscf.in | tee 2\_nscf.out \hfill 4.36\,s}
  \end{frame}

  \begin{frame}{Electrons of TaS$_2$}
    {\step 3 \textit{bands.x} --- Calculate band structure}
    \lstinputlisting{../2_TaS2/1_electrons/3_bands.in}
    \doc{www.quantum-espresso.org/Doc/INPUT\_BANDS.html}
    \footer{\sh: mpirun -np 20 bands.x -nk 10 < 3\_bands.in | tee 3\_bands.out \hfill 0.78\,s}
  \end{frame}

  \begin{frame}{Electrons of TaS$_2$}
    {\step 4 Plot band structure}
    \lstinputlisting[language=Python]{../2_TaS2/1_electrons/4_plot.py}
    \doc{bitbucket.org/berges/elphmod}
    \footer{\sh: python 4\_plot.py}
    \popup<2>{fig/tas2-el.pdf}
  \end{frame}

  \subsection{Phonons}

  \begin{frame}{Phonons of TaS$_2$}
    \begin{enumerate}
      \item Calculate charge-density self-consistently on full Brillouin zone
      \item Calculate dynamical matrices from response to atomic displacements
      \item Transform from dynamical matrices to interatomic force constants
      \vspace*{1cm}
      \item Calculate dispersion with polarization and density of states externally
    \begin{tikzpicture}[remember picture, overlay]
      \node [draw=2, color=2, rotate=20, fill=white, rounded corners, inner sep=2mm, align=center, above=5mm]
        at (current page.center)
        { Same procedure as before };
    \end{tikzpicture}
    \end{enumerate}
  \end{frame}

  \begin{frame}{Phonons of TaS$_2$}
      {\step 1 \textit{pw.x} --- Self-consistent DFT calculation}
    \vspace*{-4mm}
    \begin{minipage}[t]{0.4\textwidth}
      \lstinputlisting[lastline=20]{../2_TaS2/2_phonons/1_pw.in}
    \end{minipage}%
    \begin{minipage}[t]{0.6\textwidth}
      \lstinputlisting[firstline=21]{../2_TaS2/2_phonons/1_pw.in}
    \end{minipage}%
    \doc{www.quantum-espresso.org/Doc/INPUT\_PW.html}
    \footer{\sh: mpirun -np 20 pw.x -nk 10 < 1\_pw.in | tee 1\_pw.out \hfill 3.82\,s}
  \end{frame}

  \begin{frame}{Phonons of TaS$_2$}
      {\step 2 \textit{ph.x} --- DFPT calculation}
    \lstinputlisting{../2_TaS2/2_phonons/2_ph.in}
    \doc{www.quantum-espresso.org/Doc/INPUT\_PH.html}
    \footer{\sh: mpirun -np 20 ph.x -nk 10 < 2\_ph.in | tee 2\_ph.out \hfill 3\,min 53.82\,s}
  \end{frame}

  \begin{frame}{Phonons of TaS$_2$}
      {\step 3 \textit{q2r.x} --- Interatomic force constants from dynamical matrices}
    \lstinputlisting{../2_TaS2/2_phonons/3_q2r.in}
    \doc{q-e/PHonon/PH/q2r.f90}
    \footer{\sh: q2r.x < 3\_q2r.in | tee 3\_q2r.out \hfill 0.02\,s}
  \end{frame}

  \begin{frame}{Phonons of TaS$_2$}
      {\step 4 Plot phonons dispersion with polarization and density of states}
    \only<1>{\lstinputlisting[language=Python, linerange={1-17}]{../2_TaS2/2_phonons/4_plot.py}}%
    \only<2>{\lstinputlisting[language=Python, linerange={18-36}]{../2_TaS2/2_phonons/4_plot.py}}%
    \only<3>{\lstinputlisting[language=Python, linerange={37-48}]{../2_TaS2/2_phonons/4_plot.py}}%
    \only<4-6>{\lstinputlisting[language=Python, linerange={49-61}]{../2_TaS2/2_phonons/4_plot.py}}%
    \doc{bitbucket.org/berges/elphmod}
    \footer{\sh: mpirun -np 20 python -u 4\_plot.py}
    \popup<5>{fig/tas2-ph.pdf}
    \popup<6>{fig/tas2-ph-2.pdf}
    \begin{tikzpicture}[remember picture, overlay]
      \uncover<6>{%
        \node [draw=2, color=2, above right=3cm, rotate=-20, fill=white, rounded corners, inner sep=2mm, align=center]
          at (current page.center)
          { Smearing\\$\sigma \rightarrow 2 \times \sigma$ };
        }
    \end{tikzpicture}
  \end{frame}

  \subsection{Coupling}

  \begin{frame}{Coupling of TaS$_2$}
    \begin{enumerate}
      \item Calculate electron energies on dense mesh to resolve Fermi surface
      \item Calculate charge-density self-consistently on full Brillouin zone
      \item Calculate dynamical matrices and coupling from response to displacements
      \item Transform dynamical matrices and coupling from reciprocal to real space
      \item[5./6.] Transform both back to reciprocal space (``Fourier interpolation'')
      \item[7.] Plot dispersion with linewidths, density of states and Eliashberg function
    \begin{tikzpicture}[remember picture, overlay]
      \node [draw=2, color=2, rotate=5, fill=white, rounded corners, inner sep=2mm, align=center, below=25mm]
        at (current page.center)
        { Similar procedure as before };
    \end{tikzpicture}
    \end{enumerate}
  \end{frame}

  \begin{frame}{Coupling of TaS$_2$}
      {\step 1 \textit{pw.x} --- Dense self-consistent DFT calculation}
    \vspace*{-4mm}
    \begin{minipage}[t]{0.4\textwidth}
      \lstinputlisting[lastline=20]{../2_TaS2/3_coupling/1_pw.in}
    \end{minipage}%
    \begin{minipage}[t]{0.6\textwidth}
      \lstinputlisting[firstline=21]{../2_TaS2/3_coupling/1_pw.in}
    \end{minipage}%
    \doc{www.quantum-espresso.org/Doc/INPUT\_PW.html}
    \footer{\sh: mpirun -np 20 pw.x -nk 10 < 1\_pw.in | tee 1\_pw.out \hfill 54.12\,s}
  \end{frame}

  \begin{frame}{Coupling of TaS$_2$}
      {\step 2 \textit{pw.x} --- Coarse self-consistent DFT calculation}
    \vspace*{-4mm}
    \begin{minipage}[t]{0.4\textwidth}
      \lstinputlisting[lastline=20]{../2_TaS2/3_coupling/2_pw.in}
    \end{minipage}%
    \begin{minipage}[t]{0.6\textwidth}
      \lstinputlisting[firstline=21]{../2_TaS2/3_coupling/2_pw.in}
    \end{minipage}%
    \doc{www.quantum-espresso.org/Doc/INPUT\_PW.html}
    \footer{\sh: mpirun -np 20 pw.x -nk 10 < 2\_pw.in | tee 2\_pw.out \hfill 3.97\,s}
  \end{frame}

  \begin{frame}{Coupling of TaS$_2$}
      {\step 3 \textit{ph.x} --- DFPT calculation}
    \lstinputlisting{../2_TaS2/3_coupling/3_ph.in}
    \doc{www.quantum-espresso.org/Doc/INPUT\_PH.html}
    \footer{\sh: mpirun -np 20 ph.x -nk 10 < 3\_ph.in | tee 3\_ph.out \hfill 6\,min 52.69\,s}
  \end{frame}

  \begin{frame}{Coupling of TaS$_2$}
      {\step 4 \textit{q2r.x} --- Interatomic force constants from dynamical matrices}
    \lstinputlisting{../2_TaS2/3_coupling/4_q2r.in}
    \doc{q-e/PHonon/PH/q2r.f90}
    \footer{\sh: q2r.x < 4\_q2r.in | tee 4\_q2r.out \hfill 0.08\,s}
  \end{frame}

  \begin{frame}{Coupling of TaS$_2$}
      {\step 5 \textit{matdyn.x} --- Phonon dispersion from force constants}
    \vspace*{-4mm}
    \begin{minipage}[t][7cm]{\textwidth}
      \lstinputlisting{../2_TaS2/3_coupling/5_bnd.in}
    \end{minipage}%
    \hspace*{-0.67\textwidth}%
    \begin{minipage}[t][7cm]{0.67\textwidth}
      \vspace*{\fill}
      \begin{itemize}
        \item Phonon linewidth
        %
        \begin{align*}
          \gamma_{\vec q \nu} =
          \frac{2 \pi \omega_{\vec q \nu} \pi}{V \sub{BZ}}
          \sum_{i j} \int \D^3 k \, |g_{\vec q \vec k \nu i j}|^2
          \delta(\epsilon_{\vec k} - \mu)
          \delta(\epsilon_{\vec k + \vec q} - \mu)
        \end{align*}
      \end{itemize}
      \vspace*{\fill}
    \end{minipage}%
    \doc{q-e/PHonon/PH/matdyn.f90}
    \footer{\sh: matdyn.x < 5\_bnd.in | tee 5\_bnd.out \hfill 11.93\,s}
  \end{frame}

  \begin{frame}{Coupling of TaS$_2$}
      {\step 6 \textit{matdyn.x} --- Phonon density of states from force constants}
    \vspace*{-4mm}
    \begin{minipage}[t][7cm]{\textwidth}
      \lstinputlisting{../2_TaS2/3_coupling/6_dos.in}
    \end{minipage}%
    \hspace*{-0.67\textwidth}%
    \begin{minipage}[t][7cm]{0.67\textwidth}
      \vspace*{\fill}
      \begin{itemize}
        \item Eliashberg spectral function
        %
        \begin{align*}
          \alpha^2 F(\omega) = \frac 1 {2 \pi N(\mu)}
          \sum_{\vec q \nu} \frac{\gamma_{\vec q \nu}}{\omega_{\vec q \nu}}
          \delta(\omega_{\vec q \nu} - \omega)
        \end{align*}
      \end{itemize}
      \vspace*{\fill}
    \end{minipage}%
    \doc{q-e/PHonon/PH/matdyn.f90}
    \footer{\sh: matdyn.x < 6\_dos.in | tee 6\_dos.out \hfill 21.75\,s}
  \end{frame}

  \begin{frame}{Coupling of TaS$_2$}
      {\step 7 Plot phonons dispersion with polarization and density of states}
    \only<1>{\lstinputlisting[language=Python, linerange={1-17}]{../2_TaS2/3_coupling/7_plot.py}}%
    \only<2-3>{\lstinputlisting[language=Python, linerange={18-37}]{../2_TaS2/3_coupling/7_plot.py}}%
    \doc{bitbucket.org/berges/elphmod}
    \footer{\sh: mpirun -np 20 python -u 7\_plot.py}
    \popup<3>{fig/tas2-elph.pdf}
  \end{frame}
\end{document}
